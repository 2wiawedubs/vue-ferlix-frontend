import { defineStore } from 'pinia'

export const useAuthStore = defineStore('auth',{
    state: () => {
        const auth = localStorage.getItem('auth');
        
        if (auth)
            return JSON.parse(auth);
      
        return {
            isAuthenticated: false,
            email: null,
            access_token: null
        }
    },
    actions: {
        async login(email, password) {
            try {
                const response = await(await fetch('https://ferlix.herokuapp.com/api/login/', {
                    method: 'POST',
                    headers: { 'Content-Type': 'application/json'},
                    body: JSON.stringify({ email, password, remember_me: true })
                })).json();
                console.log();
                if (response.token) {
                    this.isAuthenticated = true;
                    this.email = email;
                    this.access_token = response.token;
            
                    localStorage.setItem('auth', JSON.stringify({ isAuthenticated: this.isAuthenticated, access_token: this.access_token, email: this.email }));
                }
            } catch (error) {
                this.logout();
            }
        },
        logout(){
            this.isAuthenticated = false;
            this.email = null;
            this.token = null;

            localStorage.removeItem('auth');
        }
    }
})
